import {NavItemName} from "../types/nav-items";

export const navItems = [NavItemName.User, NavItemName.Privacy, NavItemName.Done];
