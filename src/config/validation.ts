export const userForm = {
  rules: {
    name: "required",
    email: "required|email",
    password: ["required", "regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[\\d]).{10,}$/"],
  },
  messages: {
    "regex.password": "The password is too weak.",
  },
};
