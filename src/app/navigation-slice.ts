import produce from "immer";
import {createSlice} from "@reduxjs/toolkit";
import {NavItemName, NavItemsStates} from "../types/nav-items";
import {AllNavItemStateSchema, NavStateSchema} from "../types/nav-state-schema";
import {navItems} from "../config/nav-items";

/**
 * Generates a fresh array of nav item state objects on initialisation and every time a navigation event occurs
 *
 * @param activeIndex
 */
const generateNavItemsState = (activeIndex: number = 0) => {
  const navItemsInitialState: AllNavItemStateSchema = [];

  navItems.forEach((name, index) => {
    let state: NavItemsStates = NavItemsStates.Disabled;
    if (activeIndex === index) state = NavItemsStates.Active;
    if (activeIndex > index) state = NavItemsStates.Complete;
    navItemsInitialState.push({name, state, index});
  });

  return navItemsInitialState;
};

const initialState: NavStateSchema = {
  currentActiveNavItem: NavItemName.User,
  navItems: generateNavItemsState(),
};

export const navigationSlice = createSlice({
  name: "navigation",
  initialState,
  reducers: {
    navigate: (state, {payload}) => {
      const targetNavItem = navItems[payload];

      if (targetNavItem === state.currentActiveNavItem) {
        return state;
      }

      if (payload < 0 || payload > navItems.length - 1) {
        console.warn("Nav item does not exist at given index", {index: payload});
        return state;
      }

      return produce(state, (draftState) => {
        draftState.currentActiveNavItem = targetNavItem;
        draftState.navItems = generateNavItemsState(payload);
        return draftState;
      });
    },
  },
});

export const {navigate} = navigationSlice.actions;

export default navigationSlice.reducer;
