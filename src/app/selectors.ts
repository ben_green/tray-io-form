import {createSelector} from "@reduxjs/toolkit";
import {NavStateSchema} from "../types/nav-state-schema";
import {PrivacyFormSchema, UserFormSchema} from "../types/form-schemas";

/**
 * Retrieve currently active nav item
 */
export const currentNavItemSelector = createSelector(
  (state: {nav: NavStateSchema}) => state.nav,
  ({currentActiveNavItem}) => currentActiveNavItem
);

/**
 * Retrieve state of all nav items
 */
export const navItemsSelector = createSelector(
  (state: {nav: NavStateSchema}) => state.nav,
  ({navItems}) => navItems
);

/**
 * Retrieve state of user form
 */
export const userFormDataSelector = createSelector(
  (state: {userForm: UserFormSchema}) => state.userForm,
  (state: UserFormSchema) => state
);

/**
 * Retrieve state of privacy form
 */
export const privacyFormDataSelector = createSelector(
  (state: {privacyForm: PrivacyFormSchema}) => state.privacyForm,
  (state: PrivacyFormSchema) => state
);
