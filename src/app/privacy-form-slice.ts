import produce from "immer";
import {createSlice} from "@reduxjs/toolkit";
import {PrivacyFormSchema} from "../types/form-schemas";

const initialState: PrivacyFormSchema = {
  valid: false,
  errors: {},
  data: {
    updates: false,
    otherProducts: false,
  },
};

export const privacyFormSlice = createSlice({
  name: "privacy-form",
  initialState,
  reducers: {
    /**
     * Mutate privacy form data when user changes an input field value
     *
     * @param state
     * @param payload
     */
    updateData: (state, {payload}) => {
      return produce(state, (draftState: PrivacyFormSchema) => {
        const {data, errors, valid} = draftState;
        const {name, value} = payload;
        return {valid, errors, data: {...data, [name]: value}};
      });
    },
  },
});

export const {updateData} = privacyFormSlice.actions;

export default privacyFormSlice.reducer;
