import produce from "immer";
import {createSlice} from "@reduxjs/toolkit";
import {UserFormSchema} from "../types/form-schemas";

const initialState: UserFormSchema = {
  valid: false,
  errors: {},
  data: {
    name: "",
    role: "",
    email: "",
    password: "",
  },
};

export const userFormSlice = createSlice({
  name: "user-form",
  initialState,
  reducers: {
    /**
     * Mutate user form data when user changes an input field value
     *
     * @param state
     * @param payload
     */
    updateData: (state, {payload}) => {
      return produce(state, (draftState: UserFormSchema) => {
        const {data, errors, valid} = draftState;
        const {name, value} = payload;
        return {valid, errors, data: {...data, [name]: value.trim()}};
      });
    },

    /**
     * Mutate user form error data on validation
     *
     * @param state
     * @param payload
     */
    updateErrors: (state, {payload}) => {
      return produce(state, (draftState: UserFormSchema) => {
        const {data, errors, valid: currentValidState} = draftState;
        return {
          data,
          valid: !!payload.valid && !payload.isPartial ? payload.valid : currentValidState,
          errors: !payload.isPartial
            ? payload.errors
            : {
                ...errors,
                ...payload.errors,
              },
        };
      });
    },
  },
});

export const {updateData, updateErrors} = userFormSlice.actions;

export default userFormSlice.reducer;
