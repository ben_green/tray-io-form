import {configureStore} from "@reduxjs/toolkit";
import navReducer from "./navigation-slice";
import userFormReducer from "./user-form-slice";
import privacyFormReducer from "./privacy-form-slice";

export const store = configureStore({
  reducer: {
    nav: navReducer,
    userForm: userFormReducer,
    privacyForm: privacyFormReducer,
  },
});
