import styles from "./form.module.scss";
import {useCallback} from "react";

interface InputCheckboxProps {
  label: string;
  name: string;
  checked: boolean;
  onChange: (value: boolean) => void;
}

export default function InputCheckbox({label, name, onChange, checked}: InputCheckboxProps) {
  const handleChange = useCallback(
    (event) => {
      onChange(event.target.checked);
    },
    [onChange]
  );

  return (
    <label className={`${styles.label} ${styles.labelForCheckbox}`}>
      <input
        data-testid={`input-${name}`}
        type="checkbox"
        value="yes"
        name={name}
        onChange={handleChange}
        className={`${styles.input} ${checked ? styles.checkboxChecked : ""}`}
      />
      <span>{label}</span>
    </label>
  );
}
