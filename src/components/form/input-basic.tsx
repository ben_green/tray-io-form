import {useCallback} from "react";
import styles from "./form.module.scss";

interface InputBasicProps {
  label: string;
  name: string;
  defaultValue?: string;
  required?: boolean;
  type?: string;
  error?: string;
  onBlur?: (name: string, value: string) => void;
  onChange: (value: string) => void;
}

export default function InputBasic({
  label,
  name,
  defaultValue,
  required = true,
  type = "text",
  error,
  onBlur,
  onChange,
}: InputBasicProps) {
  const handleChange = useCallback(
    (event) => {
      onChange(event.target.value);
    },
    [onChange]
  );

  const handleBlur = useCallback(
    (event) => {
      const {value} = event.target;
      onBlur && onBlur(name, value);
    },
    [name, onBlur]
  );

  return (
    <label className={`${styles.label} ${styles.labelForBasic}`}>
      <span>
        {label} {required && <span className={styles.requirementAsterix}>*</span>}
      </span>
      <input
        data-testid={`input-${name}`}
        type={type}
        name={name}
        defaultValue={defaultValue}
        required={required}
        onChange={handleChange}
        onBlur={handleBlur}
        className={styles.input}
      />
      {!!error && <span className={styles.error}>{error}</span>}
    </label>
  );
}
