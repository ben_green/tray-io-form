import {ReactChild} from "react";
import formStepStyles from "../form-steps/form-step.module.scss";
import formStyles from "./form.module.scss";

interface FormProps {
  onSubmit: () => void;
  children: ReactChild | ReactChild[];
}

export default function Form({children, onSubmit}: FormProps) {
  return (
    <form method="post" className={formStepStyles.formStep}>
      {children}
      <div className={formStyles.footer}>
        <button type="submit" onClick={onSubmit} className={formStyles.submit}>
          Submit
        </button>
      </div>
    </form>
  );
}
