import {useCallback, useEffect, useMemo} from "react";
import {useDispatch, useSelector} from "react-redux";
import {navigate} from "../../app/navigation-slice";
import {AllNavItemStateSchema, NavItemStateSchema} from "../../types/nav-state-schema";
import {NavItemName, NavItemsStates} from "../../types/nav-items";
import {navItemsSelector} from "../../app/selectors";
import {navItems} from "../../config/nav-items";
import styles from "./nav.module.scss";

interface NavItemProps {
  index: number;
  name: NavItemName;
}

export default function NavItem({index, name}: NavItemProps) {
  const navItemsState: AllNavItemStateSchema = useSelector(navItemsSelector);
  const dispatch = useDispatch();

  /**
   * Retrieve state nav item specified by `name` prop
   */
  const navItem = useMemo((): NavItemStateSchema | undefined => {
    return navItemsState.find((item) => name === item.name);
  }, [navItemsState, name]);

  /**
   * Return style modifier for nav item
   */
  const styleModifier = useMemo((): string => {
    if (!navItem) {
      return "";
    }

    const styleModifiers = {
      [NavItemsStates.Active]: styles.navItem__active,
      [NavItemsStates.Complete]: styles.navItem__complete,
      [NavItemsStates.Disabled]: styles.navItem__disabled,
    };

    return styleModifiers[navItem?.state] ?? "";
  }, [navItem]);

  /**
   * Dispatch navigate event for requested index
   */
  const handleItemPress = useCallback((): void => {
    dispatch(navigate(index));
  }, [dispatch, index]);

  /**
   * Just in case developer passes mismatching index and name props then they should be warned
   */
  useEffect(() => {
    if (name === navItems[index]) {
      return;
    }

    console.warn("Evaluated nav item found at given index does not match given name", {
      navItem: navItems[index],
      name,
      index,
    });
  }, [name, index]);

  return (
    <li className={`${styles.navItem} ${styleModifier}`}>
      <button type="button" onClick={handleItemPress} disabled={navItem?.state === NavItemsStates.Disabled}>
        {name}
      </button>
    </li>
  );
}
