import NavItem from "./nav-item";
import {navItems} from "../../config/nav-items";
import styles from "./nav.module.scss";

export default function Nav() {
  return (
    <ul className={styles.nav}>
      {navItems.map((name, index) => (
        <NavItem name={name} index={index} key={`nav-item--${name}`} />
      ))}
    </ul>
  );
}
