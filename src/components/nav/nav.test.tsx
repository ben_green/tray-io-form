import React from "react";
import {render} from "@testing-library/react";
import {Provider} from "react-redux";
import {store} from "../../app/store";
import Nav from "./nav";

test("renders user step", () => {
  const {getByText} = render(
    <Provider store={store}>
      <Nav />
    </Provider>
  );

  expect(getByText(/User/)).toBeInTheDocument();
});

test("renders privacy step", () => {
  const {getByText} = render(
    <Provider store={store}>
      <Nav />
    </Provider>
  );

  expect(getByText(/Privacy/)).toBeInTheDocument();
});

test("renders done step", () => {
  const {getByText} = render(
    <Provider store={store}>
      <Nav />
    </Provider>
  );

  expect(getByText(/Done/)).toBeInTheDocument();
});

test("user step is active", () => {
  const {getByText} = render(
    <Provider store={store}>
      <Nav />
    </Provider>
  );

  expect(getByText(/User/)).toBeEnabled();
});

test("privacy step is disabled", () => {
  const {getByText} = render(
    <Provider store={store}>
      <Nav />
    </Provider>
  );

  expect(getByText(/Privacy/)).toBeDisabled();
});

test("done step is disabled", () => {
  const {getByText} = render(
    <Provider store={store}>
      <Nav />
    </Provider>
  );

  expect(getByText(/Done/)).toBeDisabled();
});
