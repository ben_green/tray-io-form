import {useSelector} from "react-redux";
import {currentNavItemSelector, privacyFormDataSelector, userFormDataSelector} from "../../app/selectors";
import {NavItemName} from "../../types/nav-items";
import formStepStyles from "../form-steps/form-step.module.scss";
import doneStyles from "./done.module.scss";

export default function Done() {
  const currentNavItem: NavItemName = useSelector(currentNavItemSelector);
  const {data: privacy} = useSelector(privacyFormDataSelector);
  const {data: user} = useSelector(userFormDataSelector);

  if (currentNavItem !== NavItemName.Done) {
    return null;
  }

  console.log(JSON.stringify({...user, ...privacy}));
  return (
    <div className={formStepStyles.formStep}>
      <span className={doneStyles.checkmark}>&#10003;</span>
      <p className={doneStyles.text}>
        Please verify your email address, you should have received an email from us already!
      </p>
    </div>
  );
}
