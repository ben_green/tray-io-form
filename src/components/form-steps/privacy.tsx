import {useCallback} from "react";
import {useDispatch, useSelector} from "react-redux";
import {updateData} from "../../app/privacy-form-slice";
import {navigate} from "../../app/navigation-slice";
import Form from "../form/form";
import InputCheckbox from "../form/input-checkbox";
import useInputValueChange from "../../hooks/use-input-value";
import {currentNavItemSelector, privacyFormDataSelector} from "../../app/selectors";
import {NavItemName} from "../../types/nav-items";

export default function Privacy() {
  const currentNavItem: NavItemName = useSelector(currentNavItemSelector);
  const {data} = useSelector(privacyFormDataSelector);
  const handleChange = useInputValueChange(updateData);
  const dispatch = useDispatch();

  const handleSubmit = useCallback(() => {
    dispatch(navigate(2));
  }, [dispatch]);

  if (currentNavItem !== NavItemName.Privacy) {
    return null;
  }

  return (
    <Form onSubmit={handleSubmit}>
      <InputCheckbox
        name="updates"
        label="Receive updates about Tray.io product by email"
        onChange={handleChange("updates")}
        checked={data.updates}
      />
      <InputCheckbox
        name="otherProducts"
        label="Receive communication by email for other products created by the Tray.io team"
        onChange={handleChange("otherProducts")}
        checked={data.otherProducts}
      />
    </Form>
  );
}
