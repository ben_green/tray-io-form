import React from "react";
import {render, fireEvent} from "@testing-library/react";
import {Provider} from "react-redux";
import {store} from "../../app/store";
import User from "./user";

test("basic input updates data", () => {
  const {getByTestId} = render(
    <Provider store={store}>
      <User />
    </Provider>
  );

  const value = "John Doe";
  const input = getByTestId("input-name");
  fireEvent.change(input, {target: {value}});
  expect(store.getState().userForm.data.name).toEqual(value);
});
