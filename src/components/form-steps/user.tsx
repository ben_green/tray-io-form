import {useCallback} from "react";
import {useDispatch, useSelector} from "react-redux";
import {updateData, updateErrors} from "../../app/user-form-slice";
import {navigate} from "../../app/navigation-slice";
import Form from "../form/form";
import InputBasic from "../form/input-basic";
import useInputValueChange from "../../hooks/use-input-value";
import {currentNavItemSelector, userFormDataSelector} from "../../app/selectors";
import {userForm} from "../../config/validation";
import {NavItemName} from "../../types/nav-items";
const Validator = require("validatorjs");
const lodash = require("lodash");

export default function User() {
  const currentNavItem: NavItemName = useSelector(currentNavItemSelector);
  const {errors, data} = useSelector(userFormDataSelector);
  const handleChange = useInputValueChange(updateData);
  const dispatch = useDispatch();

  const setErrors = useCallback(
    (errors, valid, isPartial = false) => {
      const newErrors = {};
      for (const [key, errorGroup] of Object.entries(errors)) {
        lodash.set(newErrors, key, lodash.head(errorGroup));
      }
      dispatch(updateErrors({errors: newErrors, valid, isPartial}));
    },
    [dispatch]
  );

  const validateAll = useCallback(() => {
    const validation = new Validator(data, userForm.rules, userForm.messages);
    const valid = validation.passes();
    setErrors(validation.errors.all(), valid);
    if (!valid) {
      return;
    }

    dispatch(navigate(1));
  }, [dispatch, setErrors, data]);

  const validatePartial = useCallback(
    (name, value) => {
      const validation = new Validator({[name]: value}, {[name]: lodash.get(userForm.rules, name)}, userForm.messages);
      const valid = validation.passes();
      setErrors(validation.errors.all(), valid, true);
    },
    [setErrors]
  );

  if (currentNavItem !== NavItemName.User) {
    return null;
  }

  return (
    <Form onSubmit={validateAll}>
      <InputBasic
        name="name"
        label="name"
        defaultValue={data.name}
        error={errors?.name}
        onBlur={validatePartial}
        onChange={handleChange("name")}
      />
      <InputBasic
        name="role"
        label="role"
        defaultValue={data.role}
        required={false}
        error={errors?.role}
        onBlur={validatePartial}
        onChange={handleChange("role")}
      />
      <InputBasic
        name="email"
        label="email"
        defaultValue={data.email}
        type="email"
        error={errors?.email}
        onBlur={validatePartial}
        onChange={handleChange("email")}
      />
      <InputBasic
        name="password"
        label="password"
        type="password"
        defaultValue={data.password}
        error={errors?.password}
        onBlur={validatePartial}
        onChange={handleChange("password")}
      />
    </Form>
  );
}
