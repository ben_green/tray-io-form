import React from "react";
import {render, fireEvent} from "@testing-library/react";
import {Provider} from "react-redux";
import {store} from "../../app/store";
import Privacy from "./privacy";
import {navigate} from "../../app/navigation-slice";

test("checkbox input updates data", () => {
  store.dispatch(navigate(1));
  const {getByTestId} = render(
    <Provider store={store}>
      <Privacy />
    </Provider>
  );

  const input = getByTestId("input-updates");
  fireEvent.click(input);
  expect(store.getState().privacyForm.data.updates).toEqual(true);
});
