import React from "react";
import Nav from "../nav/nav";
import User from "../form-steps/user";
import Privacy from "../form-steps/privacy";
import Done from "../form-steps/done";
import styles from "./app.module.scss";

function App() {
  return (
    <div className={styles.app}>
      <div className={styles.appContent}>
        <Nav />
        <User />
        <Privacy />
        <Done />
      </div>
    </div>
  );
}

export default App;
