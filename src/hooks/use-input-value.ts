import {useCallback} from "react";
import {useDispatch} from "react-redux";

/**
 * Hook to avoid code repetition when updating form state data
 *
 * @param reducer Redux reducer
 */
export default function useInputValueChange(reducer: Function) {
  const dispatch = useDispatch();

  /**
   * Call this callback with an input name. A function is returned which should be on every value change of that input.
   */
  return useCallback(
    (name: string) => (value: string | boolean) => {
      dispatch(reducer({name, value}));
    },
    [dispatch, reducer]
  );
}
