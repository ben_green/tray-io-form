import {NavItemName} from "./nav-items";

export interface UserFormSchema {
  valid: boolean;
  errors: {
    [key: string]: string;
  };
  data: {
    name: string;
    role?: string;
    email: string;
    password: string;
  };
}

export interface PrivacyFormSchema {
  valid: boolean;
  errors: {
    [key: string]: string;
  };
  data: {
    updates: boolean;
    otherProducts: boolean;
  };
}

export interface FormSchemas {
  [NavItemName.User]: UserFormSchema;
  [NavItemName.Privacy]: PrivacyFormSchema;
}
