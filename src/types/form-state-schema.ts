import {NavItemName} from "./nav-items";
import {FormSchemas} from "./form-schemas";

export interface FormStateSchema {
  [NavItemName.User]: FormSchemas[NavItemName.User];
  [NavItemName.Privacy]: FormSchemas[NavItemName.Privacy];
}
