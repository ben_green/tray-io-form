export enum NavItemName {
  User = "User",
  Privacy = "Privacy",
  Done = "Done",
}

export enum NavItemsStates {
  Active = "Active",
  Complete = "Complete",
  Disabled = "Disabled",
}

export interface NavItemSchema {
  name: NavItemName;
  state: NavItemsStates;
}
