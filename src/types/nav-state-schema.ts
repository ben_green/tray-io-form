import {NavItemName, NavItemsStates} from "./nav-items";

export type NavItemStateSchema = {
  index: number;
  state: NavItemsStates;
};

export type AllNavItemStateSchema = {name: NavItemName; state: NavItemsStates; index: number}[];

export interface NavStateSchema {
  currentActiveNavItem: NavItemName;
  navItems: AllNavItemStateSchema;
}
