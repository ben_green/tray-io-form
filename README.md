# Tray.io Coding Challenge

## Quick start

```bash
npm install && npm start
```

## Viewing a compiled version on a server

```bash
npm run build
```

It's advisable to use a node script runner such as forever / pm2, using the `./build` directory as the root
